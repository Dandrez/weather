package com.example.weatherapp

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface WeatherService {

    @GET("/data/{api_version}/weather")
    fun getWeather(
        @Path("api_version") version:String = "2.5",
        @Query("q") city:String,
        @Query("APPID") appId:String = "643eff4f173207f1c4f9576bd9678e8f"
    ): Call<WeatherResponse>

    companion object{
        val instace: WeatherService by lazy{
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            retrofit.create<WeatherService>(WeatherService::class.java)
        }
    }
}